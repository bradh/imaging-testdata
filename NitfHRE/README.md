# JITC HRE test files

High Resolution Elevation (HRE) data, which is a DEM using NITF.

Sourced from http://www.gwg.nga.mil/ntb/baseline/docs/HRE_spec/Case1_HRE10G324642N1170747W_Uxx.hr5
http://www.gwg.nga.mil/ntb/baseline/docs/HRE_spec/Case2_HRE10G324642N1170747W_Uxx.hr5
and
http://www.gwg.nga.mil/ntb/baseline/docs/HRE_spec/Case3_HRE10G324642N1170747W_Uxx.hr5


