# imaging-testdata

This is a repository of test data for the Codice Imaging project, mostly large files.

Because git doesn't do so well on large files, we use the git-lfs extension. Ideally you wouldn't need
to clone the repository, because you can just use results such as a jar of resources.

If you do need to make a change, you'll need git-lfs. See https://git-lfs.github.com/ for more.
If you clone the repository and you get tiny files of text with a few lines, one of which looks like
a reference, or a SHA-256 checksum, git-lfs isn't set up right. 